# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlSucKhoe(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    question = scrapy.Field()
    answer = scrapy.Field()
    link = scrapy.Field()
    category = scrapy.Field()
    chu_de = scrapy.Field()

class CrawlBenh(scrapy.Item):
    #tong_quan, nguyen_nhan, trieu_chung, doi_tuong, phong_ngua, 
    #bien_phap_chan_doan, bien_phap_dieu_tri, raw_data, ten_benh, chu_de
    tong_quan = scrapy.Field()
    nguyen_nhan = scrapy.Field()
    trieu_chung = scrapy.Field()
    doi_tuong = scrapy.Field()
    phong_ngua = scrapy.Field()
    bien_phap_chan_doan = scrapy.Field()
    bien_phap_dieu_tri = scrapy.Field()
    raw_data = scrapy.Field()
    ten_benh = scrapy.Field()
    chu_de = scrapy.Field()
    title = scrapy.Field()
    link = scrapy.Field()

class CoThe(scrapy.Item):
    #vi_tri, cau_tao, chuc_nang_cong_dung, raw_data, luu_y, 
    #khai_niem, ham_luong, nhu_cau, benh_thuong_gap, chu_de
    vi_tri = scrapy.Field()
    question_vi_tri = scrapy.Field()
    cau_tao = scrapy.Field()
    question_cau_tao = scrapy.Field()
    chuc_nang_cong_dung = scrapy.Field()
    question_chuc_nang_cong_dung = scrapy.Field()
    raw_data = scrapy.Field()
    luu_y = scrapy.Field()
    khai_niem = scrapy.Field()
    question_khai_niem = scrapy.Field()
    ham_luong = scrapy.Field()
    nhu_cau = scrapy.Field()
    benh_thuong_gap = scrapy.Field()
    chu_de = scrapy.Field()
    title = scrapy.Field()
    link = scrapy.Field()
    ten = scrapy.Field()