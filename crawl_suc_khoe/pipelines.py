import mysql.connector
from mysql.connector import errorcode
from .config import *
class MySQLPipeline:

    conf = {
        'host': MYSQL_HOST,
        'user': MYSQL_USER,
        'password': MYSQL_PASS,
        'database': MYSQL_DB,
        'raise_on_warnings': True
    }

    def __init__(self, **kwargs):
        self.cnx = self.mysql_connect()

    def open_spider(self, spider):
        print("spider open")

    def check_topic(self, topic):
        cursor = self.cnx.cursor()
        check_query = "SELECT count(*) FROM chu_de WHERE chu_de = '" + topic + "'"
        cursor.execute(check_query)
        myresult = cursor.fetchall()
        for x in myresult:
            exist = x[0]
        if exist == 0:
            cursor.execute("insert into chu_de(chu_de) VALUES ('" + topic + "')")
            self.cnx.commit()
            id = cursor.lastrowid
            return id
        else:
            query_id = "SELECT id FROM chu_de WHERE chu_de = '" + topic + "'"
            cursor.execute(query_id)
            myresult = cursor.fetchall()
            if myresult is not None:
                for x in myresult:
                    id = x[0]
                return id

    def check_benh_thuong_gap(self, ten_benh):
        cursor = self.cnx.cursor()
        query_id = "SELECT id FROM benh WHERE link = '" + ten_benh + "'"
        cursor.execute(query_id)
        myresult = cursor.fetchall()
        if myresult is not None:
            for x in myresult:
                id = x[0]
                return id
        else:
            return None

    def process_item(self, item, spider):
        print("Saving item into db ...")
        self.save(dict(item), spider.name)
        # return item

    def close_spider(self, spider):
        self.mysql_close()

    def mysql_connect(self):
        try:
            return mysql.connector.connect(**self.conf)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def save(self, item, spider_name):
        cursor = self.cnx.cursor()
        id_chu_de = []
        list_chu_de = item["chu_de"]
        for chu_de in list_chu_de:
            id = self.check_topic(chu_de)
            id_chu_de.append(id)
        if spider_name == "vinmec_thuoc":
            create_query = ("insert into thuoc(ten_thuoc, dang_bao_che, nhom_thuoc_tac_dung, chi_dinh, chong_chi_dinh, than_trong, lieu_cach_dung, tai_lieu_tham_khao, raw_data, tac_dung_khong_mong_muon, link) "
                            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")

            cursor.execute(create_query,
                           (item["ten_thuoc"], item["dang_bao_che"], item["nhom_thuoc_tac_dung"], item["chi_dinh"], item["chong_chi_dinh"], item["than_trong"], item["lieu_cach_dung"], item["tai_lieu_tham_khao"], item["raw_data"], item["tac_dung_khong_mong_muon"], item["link"]))
            lastRecordId = cursor.lastrowid
            self.cnx.commit()
            for chu_de in id_chu_de:
                create_query_qh = (
                    "insert into quan_he_chu_de(id_item, id_chu_de, id_type) "
                    "VALUES (%s, %s, %s)")
                cursor.execute(create_query_qh,
                               (lastRecordId, chu_de, 3))
                self.cnx.commit()
            cursor.close()
            print("Item saved with ID: {} in {}".format(lastRecordId, spider_name))
        elif spider_name == "vinmec_su_dung_thuoc":
            import json
            create_query = (
                "insert into su_dung_thuoc(tieu_de, content, raw_data, link) "
                "VALUES (%s, %s, %s, %s)")
            cursor.execute(create_query,
                           (item["tieu_de"], json.dumps(item["content"], ensure_ascii=False), item["raw_data"],
                            item["link"]))
            lastRecordId = cursor.lastrowid
            self.cnx.commit()
            for chu_de in id_chu_de:
                create_query_qh = (
                    "insert into quan_he_chu_de(id_item, id_chu_de, id_type) "
                    "VALUES (%s, %s, %s)")
                cursor.execute(create_query_qh,
                               (lastRecordId, chu_de, 1))
                self.cnx.commit()

            cursor.close()
            print("Item saved with ID: {} in {}".format(lastRecordId, spider_name))
        elif spider_name == "vinmec_thong_tin_suc_khoe":
            import json
            create_query = (
                "insert into thong_tin_suc_khoe(tieu_de, content, raw_data, link) "
                "VALUES (%s, %s, %s, %s)")
            cursor.execute(create_query,
                           (item["tieu_de"], json.dumps(item["content"], ensure_ascii=False), item["raw_data"],
                            item["link"]))
            lastRecordId = cursor.lastrowid
            self.cnx.commit()
            for chu_de in id_chu_de:
                create_query_qh = (
                    "insert into quan_he_chu_de(id_item, id_chu_de, id_type) "
                    "VALUES (%s, %s, %s)")
                cursor.execute(create_query_qh,
                               (lastRecordId, chu_de, 2))
                self.cnx.commit()
            cursor.close()
            print("Item saved with ID: {} in {}".format(lastRecordId, spider_name))
        elif spider_name == "vinmec_benh":
            create_query = (
                "insert into benh(tong_quan, nguyen_nhan, trieu_chung, doi_tuong, phong_ngua, bien_phap_chan_doan, bien_phap_dieu_tri, raw_data, ten_benh, title, link) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
            cursor.execute(create_query,
                           (item["tong_quan"], item["nguyen_nhan"], item["trieu_chung"], item["doi_tuong"], item["phong_ngua"], item["bien_phap_chan_doan"], item["bien_phap_dieu_tri"], item["raw_data"],
                            item["ten_benh"], item["title"], item["link"]))
            lastRecordId = cursor.lastrowid
            self.cnx.commit()
            for chu_de in id_chu_de:
                create_query_qh = (
                    "insert into quan_he_chu_de(id_item, id_chu_de, id_type) "
                    "VALUES (%s, %s, %s)")
                cursor.execute(create_query_qh,
                               (lastRecordId, chu_de, 4))
                self.cnx.commit()
            cursor.close()
            print("Item saved with ID: {} in {}".format(lastRecordId, spider_name))
        elif spider_name == "co_the":
            id_benh = []
            list_benh = item["benh_thuong_gap"]
            for benh in list_benh:
                id = self.check_benh_thuong_gap(benh)
                id_benh.append(id)
            create_query = (
                "insert into co_the(vi_tri, cau_tao, chuc_nang_cong_dung, raw_data, luu_y, khai_niem, ham_luong, nhu_cau, link, ten, title, question_vi_tri, question_cau_tao, question_chuc_nang_cong_dung, question_khai_niem) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
            cursor.execute(create_query,
                           (item["vi_tri"], item["cau_tao"], item["chuc_nang_cong_dung"], item["raw_data"],
                            item["luu_y"], item["khai_niem"], item["ham_luong"],
                            item["nhu_cau"],
                             item["link"], item["ten"],  item["title"],  item["question_vi_tri"], item["question_cau_tao"], item["question_chuc_nang_cong_dung"], item["question_khai_niem"]))
            lastRecordId = cursor.lastrowid
            self.cnx.commit()
            for benh in id_benh:
                if benh is not None:
                    create_query_qh = (
                        "insert into quan_he_benh_co_the(id_benh, id_co_the) "
                        "VALUES (%s, %s)")
                    cursor.execute(create_query_qh,
                                   (benh, lastRecordId))
                    self.cnx.commit()
            for chu_de in id_chu_de:
                create_query_qh = (
                    "insert into quan_he_chu_de(id_item, id_chu_de, id_type) "
                    "VALUES (%s, %s, %s)")
                cursor.execute(create_query_qh,
                               (lastRecordId, chu_de, 5))
                self.cnx.commit()
            cursor.close()
            print("Item saved with ID: {} in {}".format(lastRecordId, spider_name))
        elif spider_name == "blog_suc_khoe" or spider_name == "vnexpress_qa":
            check_query = "SELECT count(*) FROM blog_suc_khoe WHERE link = '" + item["link"] + "'"
            cursor.execute(check_query)
            myresult = cursor.fetchall()
            for x in myresult:
                exist = x[0]
            create_query = ("insert into blog_suc_khoe(link, category, title, content, raw_data) "
                            "VALUES (%s, %s, %s, %s, %s)")
            if exist == 0:
                # Insert new row
                cursor.execute(create_query, (
                item["link"], item["category"], item["title"], item["content"], item["raw_data"]))
                lastRecordId = cursor.lastrowid

                self.cnx.commit()
                cursor.close()
                print("Item saved with ID: {}".format(lastRecordId))
            else:
                print("Đã có")
        else:
            check_query = "SELECT count(*) FROM data_qa_suc_khoe WHERE question = '" + item["question"] + "'"
            cursor.execute(check_query)
            myresult = cursor.fetchall()
            for x in myresult:
                exist = x[0]
            create_query = ("insert into data_qa_suc_khoe(link, category, question, answer, raw_data) "
                            "VALUES (%s, %s, %s, %s, %s)")
            if exist == 0:
                # Insert new row
                cursor.execute(create_query, (item["link"], item["category"], item["question"], item["answer"], item["raw_data"]))
                lastRecordId = cursor.lastrowid

                self.cnx.commit()
                cursor.close()
                print("Item saved with ID: {}".format(lastRecordId))
            else:
                print("Đã có")


    def mysql_close(self):
        self.cnx.close()

