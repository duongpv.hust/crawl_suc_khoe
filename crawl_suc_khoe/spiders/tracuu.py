import scrapy
from crawl_suc_khoe.items import CrawlSucKhoe, CoThe, CrawlBenh



class TracuuSpider(scrapy.Spider):
    name = 'tracuu'
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/benh/', 'https://vinmec.com/vi/co-the-nguoi/']
    domain = 'https://vinmec.com'

    def start_request(self):
        print(self.start_urls)
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)
           
    def parse(self, response):
        characters = ['a', 'b', 'c', 'd', 'đ', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

        total_link = []
        for c in characters:
            css = 'section#'+c+' ul.collapsible-target li a::attr(href)'
            list_link = response.css(css).getall()
            for link in list_link:
                total_link.append(link)

        for url in total_link:
            new_url = self.domain + url
            yield scrapy.Request(url=new_url, callback=self.parse_item)
    

    def parse_item(self, response):
        item = CrawlSucKhoe()
        text = ''
        item['title'] = response.css('h1::text').get().strip()
        for ele in response.css('section.collapsible-container'):
            item['question'] = ele.css('h2 span::text').get()
            for e in ele.css('div.collapsible-target p::text').getall():
                text += '\n' + e
            item['answer'] = text
            item['link'] = response.url
            item['category'] = response.css('ul.breadcrumbs li a::text').getall()[1]
            yield item


class CrawlBenhVaCoThe(scrapy.Spider):
    name = 'vinmec_benh'
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/benh/']
    domain = 'https://vinmec.com'

    def start_request(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        characters = ['a', 'b', 'c', 'd', 'đ', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

        total_link = []
        for c in characters:
            # css = 'section#'+c+' ul.collapsible-target li a::attr(href)'
            css = 'section#'+c+' ul.collapsible-target li'
            for ele in response.css(css):
                temp = {}
                link = ele.css('a::attr(href)').get()
                name = ele.css('a::text').get()
                temp['link'] = link
                temp['name'] = name
                total_link.append(temp)

        for item in total_link:
            new_url = self.domain + item['link']
            # print(new_url)
            request = scrapy.Request(url=new_url, callback=self.parse_item)
            request.meta['name'] = item['name']
            yield request

    def parse_item(self, response):
        item = CrawlBenh()
        item['tong_quan'] = ''
        item['nguyen_nhan'] = ''
        item['trieu_chung'] = ''
        item['doi_tuong'] = ''
        item['phong_ngua'] = ''
        item['bien_phap_chan_doan'] = ''
        item['bien_phap_dieu_tri'] = ''
        item['raw_data'] = response.body
        item['ten_benh'] = response.meta['name']
        item['chu_de'] = response.css('div.tags a::text').getall()
        item['link'] = response.url
        item['title'] = response.css('h1::text').get().strip()

        for ele in response.css('section.collapsible-container'):
            text = ''
            tieu_de = ele.css('h2 span::text').get()
            for e in ele.css('div.collapsible-target p::text,strong::text').getall():
                text += '\n' + e
            if tieu_de.lower().find('tổng quan') != -1:
                item['tong_quan'] = text
            if tieu_de.lower().find('nguyên nhân') != -1:
                item['nguyen_nhan'] = text
            if tieu_de.lower().find('triệu chứng') != -1:
                item['trieu_chung'] = text
            if tieu_de.lower().find('đối tượng') != -1:
                item['doi_tuong'] = text
            if tieu_de.lower().find('phòng ngừa') != -1:
                item['phong_ngua'] = text
            if tieu_de.lower().find('biện pháp chẩn đoán') != -1:
                item['bien_phap_chan_doan'] = text
            if tieu_de.lower().find('biện pháp điều trị') != -1:
                item['bien_phap_dieu_tri'] = text
        yield item

class CrawlCoThe(scrapy.Spider):
    name = 'co_the'
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/co-the-nguoi/']
    domain = 'https://vinmec.com'

    def start_request(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        characters = ['a', 'b', 'c', 'd', 'đ', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

        total_link = []
        for c in characters:
            # css = 'section#'+c+' ul.collapsible-target li a::attr(href)'
            css = 'section#'+c+' ul.collapsible-target li'
            for ele in response.css(css):
                temp = {}
                link = ele.css('a::attr(href)').get()
                name = ele.css('a::text').get()
                temp['link'] = link
                temp['name'] = name
                total_link.append(temp)

        for item in total_link:
            new_url = self.domain + item['link']
            # print(new_url)
            request = scrapy.Request(url=new_url, callback=self.parse_item)
            request.meta['name'] = item['name']
            yield request

    def parse_item(self, response):
            item = CoThe()
            item['vi_tri'] = ''
            item['cau_tao'] = ''
            item['chuc_nang_cong_dung'] = ''
            item['raw_data'] = response.body
            item['luu_y'] = ''
            item['khai_niem'] = ''
            item['ham_luong'] = ''
            item['nhu_cau'] = ''
            item['benh_thuong_gap'] = []
            item['chu_de'] = response.css('div.tags a::text').getall()
            item['title'] = response.css('h1::text').get().strip()
            item['link'] = response.url
            item['ten'] = response.meta['name']
            item['question_vi_tri'] = ""
            item['question_cau_tao'] = ""
            item['question_chuc_nang_cong_dung'] = ""
            item['question_khai_niem'] = ""

            for ele in response.css('section.collapsible-container'):
                text = ''
                tieu_de = ele.css('h2 span::text').get()
                for e in ele.css('div.collapsible-target p::text,strong::text').getall():
                    text += '\n' + e
                if tieu_de.lower().find('vị trí') != -1:
                    item['vi_tri'] = text
                    # print(tieu_de)
                    item['question_vi_tri'] = tieu_de
                if tieu_de.lower().find('cấu tạo') != -1:
                    item['cau_tao'] = text

                    item['question_cau_tao'] = tieu_de

                if tieu_de.lower().find('chức năng') != -1 or tieu_de.lower().find('công dụng') != -1:
                    item['chuc_nang_cong_dung'] = text

                    item['question_chuc_nang_cong_dung'] = tieu_de

                if tieu_de.lower().find('lưu ý') != -1:
                    item['luu_y'] = text
                if tieu_de.lower().find('khái niệm') != -1:
                    item['khai_niem'] = text

                    item['question_khai_niem'] = tieu_de

                if tieu_de.lower().find('hàm lượng') != -1:
                    item['ham_luong'] = text
                if tieu_de.lower().find('nhu cầu') != -1:
                    item['nhu_cau'] = text
                if tieu_de.lower().find('bênh thường gặp') != -1:
                    item['benh_thuong_gap'].append(text)
                if tieu_de.lower().find('vấn đề thường gặp') != -1 or tieu_de.lower().find('bệnh thường gặp') != -1:
                    benh_thuong_gap = response.css('div.collapsible-target ul li a::attr(href)').getall()
                    for benh in benh_thuong_gap:
                        url_benh = self.domain + benh
                        item['benh_thuong_gap'].append(url_benh)
            # print(response.url)
            # print(item['benh_thuong_gap'])
            # print(item)
            yield item



