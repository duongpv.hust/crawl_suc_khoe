import scrapy
from crawl_suc_khoe.items import CrawlSucKhoe
import re


class VinmecSpider(scrapy.Spider):
    name = 'vinmec'
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/tin-tuc/hoi-dap-bac-si/?page=1', 'https://vinmec.com/vi/tin-tuc/hoi-dap-bac-si/?page=2']
    domain = 'https://vinmec.com/vi'

    def start_request(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        list_urls = response.css('div.post-list li a::attr(href)').extract()
        for url in list_urls:
            new_url = self.domain +  url
            yield scrapy.Request(url= new_url, callback=self.get_content)


    def get_content(self, response):
        item = CrawlSucKhoe()
        question = ''
        answer = ''
        ls_question = response.css('div.rich-text p i::text').getall()
        ls_answer = response.css('div.rich-text p::text').getall()
        for i in ls_question:
            question += i
        for i in ls_answer:
            answer += i
        item['title'] = response.css('h1::text').get().strip()
        item['question'] = question
        item['answer'] = answer
        item['link'] = response.url
        item['category'] = response.css('div.breadcrumb a.no-underline::text').getall()[1]
        yield item