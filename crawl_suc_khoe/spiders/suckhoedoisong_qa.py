from scrapy.spiders import CrawlSpider, Rule
import scrapy

class SucKhoeDoiSongSpider(CrawlSpider):
    name = "suckhoedoisong_qa"

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)
        self.page_num = 1

    def start_requests(self):
        urls = [
            'https://suckhoedoisong.vn/phong-mach-online-c16/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, meta={"page_num": 1})

    def parse(self, response):
        page_num = response.meta["page_num"]
        list_post = response.xpath('//article[@class="d-flex"]/a/@href').getall()
        for i in range(len(list_post)):
            yield scrapy.Request(url=list_post[i], callback=self.parse_detail)
        next_page = response.xpath('//nav[@aria-label="Page navigation"]/a').getall()
        if len(next_page) > 1:
            page_num += 1
            if "/p" in response.url:
                str_del = response.url.split("/")[-1]
                url_main = response.url.replace(str_del, "")
            else:
                url_main = response.url
            yield scrapy.Request(url=url_main + "p" + str(page_num), callback=self.parse, meta={"page_num": page_num})

    def parse_detail(self, response):
        list_qa = response.xpath('//ul[@class="list_qa"]/li')
        for qa in list_qa:
            data = dict()
            text_question = qa.xpath('div[@class="box_question"]/div[@class="content_ques"]/text()').getall()
            text_answer = qa.xpath('//div[@class="box_answer mt30 mb30"]/div/div/div/p/span/text()').getall()
            category = "Hỏi đáp"
            question = ""
            answer = ''
            for text in text_question:
                question += text.strip() + " "
            for text in text_answer:
                answer += text.strip() + " "
            data.update({
                "question": question.replace("\r\n", ""),
                "answer": answer.replace("\r\n", ""),
                "link": response.url,
                "category": category,
                "raw_data": response.body,
                "chu_de": []
            })
            yield data


