from scrapy.spiders import CrawlSpider, Rule
import scrapy

class BenhVien108Spider(CrawlSpider):
    name = "benhvien108"

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)
        self.page_num = 1

    def start_requests(self):
        urls = [
            'http://benhvien108.vn/hoi-dap.htm',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, meta={"page_num": 1})

    def parse(self, response):
        list_post = response.xpath('//div[@class="info-content"]/div[@class="title"][1]/h3/a/@href').getall()
        # print(response.url)
        # print(list_post)
        for i in range(len(list_post)):
            yield scrapy.Request(url="https://benhvien108.vn" + list_post[i], callback=self.parse_detail)
        next_page = response.xpath('//div[@class="info-content"]/div[@class="title"][1]/h3/a/@href').getall()
        if next_page is not None:
            self.page_num += 1
            yield scrapy.Request(url="http://benhvien108.vn/hoi-dap.htm?" + "p=" + str(self.page_num), callback=self.parse, meta={"page_num": self.page_num})

    def parse_detail(self, response):
        data = dict()
        title = ""
        category = response.xpath('//div[@class="page-title"]/a/h1/text()').get()
        question = response.xpath('//div[@class="title"]/h3/a/text()').get()
        answer = ''
        for text in response.xpath('(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/div/text())|(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/p/span/text())|(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/p/text())|(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/div/div/text())|(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/div/p/span)|(//*[@id="BodyContent_ctl00_leftPanel"]/div[@class="page-content"]/article/div/div/div/div/div/b/text())').getall():
            answer += text.strip() + " "
        data.update({
            "title": title.lower(),
            "question": question,
            "answer": answer,
            "link": response.url,
            "category": category.lower(),
            "chu_de": [],
            "raw_data": response.body
        })
        # print(data)
        yield data


