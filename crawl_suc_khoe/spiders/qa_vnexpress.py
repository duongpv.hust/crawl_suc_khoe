from scrapy.spiders import CrawlSpider, Rule
import scrapy

class VnexpressSpider(CrawlSpider):
    name = "vnexpress_qa"

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)

    def start_requests(self):
        urls = [
            'https://vnexpress.net/suc-khoe/tu-van',
            'https://vnexpress.net/suc-khoe/tin-tuc',
            "https://vnexpress.net/suc-khoe/dinh-duong",
            "https://vnexpress.net/suc-khoe/khoe-dep",
            "https://vnexpress.net/suc-khoe/dan-ong",
            "https://vnexpress.net/suc-khoe/cac-benh",
            "https://vnexpress.net/suc-khoe/ung-thu",
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, meta={"page_num": 72})

    def parse(self, response):
        page_num = response.meta["page_num"]
        print(page_num)
        list_post = response.xpath('(//h2[@class="title-news"]/a/@href)|(//h3[@class="title_news"]/a/@href)').getall()
        # print(list_post)
        for i in range(len(list_post)):
            yield scrapy.Request(url=list_post[i], callback=self.parse_detail)
        next_page = response.xpath('(//div[@class="button-page flexbox"]/a)|(//div[@class="pagination mb10"]/div/a)').getall()
        if next_page is not None:
            page_num += 1
            if "-p" in response.url:
                str_del = response.url.split("-")[-1]
                url_main = response.url.replace(str_del, "")
            else:
                url_main = response.url + "-"
            yield scrapy.Request(url=url_main + "p" + str(page_num), callback=self.parse, meta={"page_num": page_num})

    def parse_detail(self, response):
        data = dict()
        title = response.xpath('(//h1[@class="title-detail"]/text())|(//div[@class="title_news"]/h1/text())').get()
        question = response.xpath('(//p[@class="description"]/text())|(//h3[@class="short_intro txt_666"]/text())').get()
        answer = ''
        for text in response.xpath('(//article[@class="fck_detail "]/p/text())|(//p[@class="Image"]/text())').getall():
            answer += text.strip() + " "
        content = question + answer
        data.update({
            "title": title.strip(),
            "content": content,
            "link": response.url,
            "category": response.xpath('(//ul[@class="breadcrumb"]/li[2]/a/text())|(//div[@class="width-commin2"]/ul[@class="menu--first"]/li[2]/a/text())').get(),
            "chu_de": [],
            "raw_data": response.body
        })
        yield data


