from scrapy.spiders import CrawlSpider, Rule
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from newspaper import Article
from newsplease import NewsPlease


class BlogSucKhoeSpider(CrawlSpider):
    name = "blog_suc_khoe"
    start_urls = ['https://www.blogsuckhoe.com/']
    allowed_domains = ['www.blogsuckhoe.com']
    rules = (
        Rule(LinkExtractor(allow_domains=['www.blogsuckhoe.com']), callback='parse',
             follow=True),
    )

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)
        self.page_num = 1

    def parse(self, response):
        title = response.xpath('//h1[@class="title"]/text()').get()
        if title is not None:
            # article = NewsPlease.from_url(response.url)
            text_content = ""
            content = response.xpath('(//div[@class="entry clearfix"]/p/strong/text())|(//div[@class="entry clearfix"]/p/text())|(//div[@class="entry clearfix"]/p/a/text())').getall()
            for text in content:
                text_content += text
            # print(text.strip())
            category = response.xpath('//span[@class="meta_categories"]/a/text()').get()
            data = dict()
            data.update({
                "title": title.strip(),
                "link": response.url,
                "category": category,
                "content": text_content,
                "raw_data": response.body,
                "chu_de": []
            })
            yield data