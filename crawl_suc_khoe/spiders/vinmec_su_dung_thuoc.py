import scrapy
import re

def parse_text(list):
    content = ""
    for text in list:
        content += text + " "
    return content

def check_cate(list_word_in_cate, cate):
    for i in list_word_in_cate:
        if i in cate:
            return True
            break

def no_accent_vietnamese(s):
    s = re.sub('[áàảãạăắằẳẵặâấầẩẫậ]', 'a', s)
    s = re.sub('[ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ]', 'A', s)
    s = re.sub('[éèẻẽẹêếềểễệ]', 'e', s)
    s = re.sub('[ÉÈẺẼẸÊẾỀỂỄỆ]', 'E', s)
    s = re.sub('[óòỏõọôốồổỗộơớờởỡợ]', 'o', s)
    s = re.sub('[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]', 'O', s)
    s = re.sub('[íìỉĩị]', 'i', s)
    s = re.sub('[ÍÌỈĨỊ]', 'I', s)
    s = re.sub('[úùủũụưứừửữự]', 'u', s)
    s = re.sub('[ÚÙỦŨỤƯỨỪỬỮỰ]', 'U', s)
    s = re.sub('[ýỳỷỹỵ]', 'y', s)
    s = re.sub('[ÝỲỶỸỴ]', 'Y', s)
    s = re.sub('đ', 'd', s)
    s = re.sub('Đ', 'D', s)
    s = s.replace(" ", "_")
    s = s.lower()
    for i in range(10):
        str_del = str(i) + "._"
        if str_del in s:
            s = s.replace(str_del, "")
    if "___" in s:
        s = s.replace("___", "")
    if "____" in s:
        s = s.replace("____", "")
    return s.strip()

class SuDungThuocVinmecSpider(scrapy.Spider):
    name = 'vinmec_su_dung_thuoc'
    type_name = "su_dung_thuoc"
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/thong-tin-duoc/su-dung-thuoc-toan/']

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)

    def start_request(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # print(response.url)
        list_url = response.xpath('//div[@class="post-list"]/ul/li/div/h2/a/@href').getall()
        for url in list_url:
            yield scrapy.Request(url="https://vinmec.com/vi" + url, callback=self.parse_detail_medicine)
        for i in range(48):
            yield scrapy.Request(url="https://vinmec.com/vi/thong-tin-duoc/su-dung-thuoc-toan/?page=" + str(i+2), callback=self.parse)

    def parse_detail_medicine(self, response):
        data_dict = dict()
        data = dict()
        print(response.url)
        list_cate = response.xpath('(//div[@class="block-content cms"]/h2/text())|(//h2/b/text())').getall()
        if list_cate is not None:
            for i in range(len(list_cate)):
                data.update({
                    no_accent_vietnamese(list_cate[i]) : {
                        "content": list_cate[i],
                        "main": parse_text(response.xpath('(//div[@class="rich-text"][' + str(i+2) + ']/p/text())|(//div[@class="rich-text"][' + str(i+2) + ']/p/a/b/text())|(//div[@class="rich-text"][' + str(i+2) + ']/p/b/text())|(//div[@class="rich-text"][' + str(i+2) + ']/ul/li/text())|(//div[@class="rich-text"][' + str(i+2) + ']/ul/li/a/b/text())').getall())
                    }
                })
            data_chu_de = []
            list_chu_de = response.xpath('//div[@class="tags"]/a/text()').getall()
            for chu_de in list_chu_de:
                data_chu_de.append(chu_de)
            data_dict.update({
                "content": data,
                "link": response.url,
                "chu_de": data_chu_de,
                "raw_data": response.body,
                "tieu_de": response.xpath("//h1/text()").get()
            })
            import json
            yield data_dict
        elif response.xpath('(//div[@class="rich-text"]/p/text())|(//div[@class="rich-text"]/ul/li/b/text())|(//div[@class="rich-text"]/ul/li/text())|(//div[@class="rich-text"]/ul/li/a/b/text())').getall() is not None:
            data = ""
            list_text = response.xpath(
                '(//div[@class="rich-text"]/p/text())|(//div[@class="rich-text"]/ul/li/b/text())|(//div[@class="rich-text"]/ul/li/text())|(//div[@class="rich-text"]/ul/li/a/b/text())').getall()
            for text in list_text:
                data += text
            data_chu_de = []
            list_chu_de = response.xpath('//div[@class="tags"]/a/text()').getall()
            for chu_de in list_chu_de:
                data_chu_de.append(chu_de)
            data_dict.update({
                "content": data,
                "link": response.url,
                "chu_de": data_chu_de,
                "raw_data": response.body,
                "tieu_de": response.xpath("//h1/text()").get()
            })
            yield data_dict
        else:
            pass