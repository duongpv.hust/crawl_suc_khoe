import scrapy


def parse_text(list):
    content = ""
    for text in list:
        content += text + " "
    return content


class ThuocVinmecSpider(scrapy.Spider):
    name = 'vinmec_thuoc'
    type_name = "thuoc"
    allowed_domains = ['vinmec.com']
    start_urls = ['https://vinmec.com/vi/thuoc/']

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)

    def start_request(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # print(response.url)
        list_url = response.xpath('//ul[@class="o-masonry o-masonry--column-3"]/li/a/@href').getall()
        for url in list_url:
            yield scrapy.Request(url="https://vinmec.com" + url, callback=self.parse_detail_medicine)
        for i in range(6):
            yield scrapy.Request(url="https://vinmec.com/vi/thuoc/?page=" + str(i+2), callback=self.parse)

    def parse_detail_medicine(self, response):
        data = dict()
        ten_thuoc = response.xpath("//h1/text()").get()
        dang_bao_che = parse_text(response.xpath('//section[@class="formulation_and_brand collapsible-container collapsible-block expanded hide-other-content clear-float"]/div/p/text()').getall())
        nhom_thuoc_tac_dung = parse_text(response.xpath('//section[@class="drug_type_free collapsible-container collapsible-block expanded hide-other-content clear-float"]/div/p/text()').getall())
        chi_dinh = parse_text(response.xpath('(//section[@class="uses collapsible-container collapsible-block expanded hide-other-content clear-float"]/div/p/text())|(//section[@class="uses collapsible-container collapsible-block expanded hide-other-content clear-float"]/div/p/a/text())|(//section[@class="uses collapsible-container collapsible-block expanded hide-other-content clear-float"]/div/p/em/text())').getall())
        chong_chi_dinh = parse_text(response.xpath('(//section[@class="contraindication collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())').getall())
        than_trong = parse_text(response.xpath('(//section[@class="warning2 collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())|(//section[@class="warning2 collapsible-container collapsible-block expanded hide-other-content"]/div/p/em/text())').getall())
        tac_dung_khong_mong_muon = parse_text(response.xpath('(//section[@class="unwanted_effect collapsible-container collapsible-block hide-other-content collapsed"]/div/p/text())|(//section[@class="unwanted_effect collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())|(//section[@class="unwanted_effect collapsible-container collapsible-block expanded hide-other-content"]/div/p/em/text())').getall())
        lieu_cach_dung = parse_text(response.xpath('(//section[@class="instructions collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())|(//section[@class="instructions collapsible-container collapsible-block expanded hide-other-content"]/div/p/em/text())').getall())
        chu_y = parse_text(response.xpath('(//section[@class="caution collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())|(//section[@class="caution collapsible-container collapsible-block expanded hide-other-content"]/div/p/em/text())|(//section[@class="caution collapsible-container collapsible-block expanded hide-other-content"]/div/p/strong/em/text())').getall())
        tai_lieu = parse_text(response.xpath('(//section[@class="references collapsible-container collapsible-block expanded hide-other-content"]/div/p/text())|(//section[@class="references collapsible-container collapsible-block expanded hide-other-content"]/div/p/em/text())').getall())
        list_chu_de = response.xpath('//div[@class="tags"]/a/text()').getall()
        data_chu_de = []
        for chu_de in list_chu_de:
            data_chu_de.append(chu_de)
        data.update({
            "ten_thuoc": ten_thuoc.strip(),
            "dang_bao_che": dang_bao_che,
            "nhom_thuoc_tac_dung": nhom_thuoc_tac_dung,
            "chi_dinh": chi_dinh,
            "chong_chi_dinh": chong_chi_dinh,
            "than_trong": than_trong,
            "tac_dung_khong_mong_muon": tac_dung_khong_mong_muon,
            "lieu_cach_dung": lieu_cach_dung,
            "chu_y": chu_y,
            "tai_lieu_tham_khao": tai_lieu,
            "chu_de": data_chu_de,
            "raw_data": response.body,
            "link": response.url
        })
        print(response.url)
        yield (data)