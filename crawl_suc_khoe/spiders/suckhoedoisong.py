from scrapy.spiders import CrawlSpider, Rule
import scrapy

class SucKhoeDoiSongSpider(CrawlSpider):
    name = "suckhoedoisong"

    def __init__(self, crawlMode='', **kwargs):
        super().__init__(**kwargs)
        self.page_num = 1

    def start_requests(self):
        urls = [
            'https://suckhoedoisong.vn/y-hoc-co-truyen-c9/',
            # 'https://suckhoedoisong.vn/y-hoc-360-c1/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, meta={"page_num": 1})

    def parse(self, response):
        page_num = response.meta["page_num"]
        list_post = response.xpath('//article[@class="d-flex"]/a/@href').getall()
        for i in range(len(list_post)):
            yield scrapy.Request(url=list_post[i], callback=self.parse_detail_y_hoc)
        next_page = response.xpath('//nav[@aria-label="Page navigation"]/a').getall()
        if len(next_page) > 1:
            page_num += 1
            if "/p" in response.url:
                str_del = response.url.split("/")[-1]
                url_main = response.url.replace(str_del, "")
            else:
                url_main = response.url
            yield scrapy.Request(url=url_main + "p" + str(page_num), callback=self.parse, meta={"page_num": page_num})

    def parse_detail_y_hoc(self, response):
        print(response.url)
        data = dict()
        title = response.xpath('(//div[@class="title-detail xh-highlight"]/text())|(//div[@class="title-detail"]/text())').get()
        category = response.xpath('//div[@class="fs16 text-uppercase"]/a/text()').get()
        question = response.xpath('//div[@class="sapo_detail"]/text()').get()
        answer = ''
        for text in response.xpath('//div[@itemprop="articleBody"]/p/text()').getall():
            answer += text.strip() + " "
        raw_content = ""
        for text in response.xpath('(//div[@itemprop="articleBody"]/div/strong/em/text())|(//div[@itemprop="articleBody"]/div/text())').getall():
            raw_content += text.strip() + " "
        data.update({
            "title": title.lower(),
            "question": question.strip(),
            "answer": answer,
            "link": response.url,
            "category": category.lower(),
            "chu_de": [],
            "raw_content": raw_content
        })
        # print(data)
        yield data


